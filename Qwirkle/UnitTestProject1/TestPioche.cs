﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Testpieces()
        {
            QColor red = QColor.red;
            QColor yellow = QColor.yellow;
            QShape cross = QShape.cross;
            QShape clover = QShape.clover;
            Piece redcross = new Piece (red,cross);
            Assert.AreEqual(cross, redcross.getform());
            Assert.AreEqual(red, redcross.getcolor());
            Assert.AreNotEqual(yellow, redcross.getcolor());
            Assert.AreNotEqual(clover, redcross.getform());
        }

        [TestMethod]
        public void TestCompability()
        {
            QColor red = QColor.red;
            QColor yellow = QColor.yellow;
            QShape cross = QShape.cross;
            QShape diamond = QShape.diamond;
            Piece redcross = new Piece(red, cross);
            Piece yellowclover = new Piece(yellow, diamond);
            Piece redclover = new Piece(red, diamond);
            Assert.AreEqual(true, redcross.Compatibility(redclover));
            Assert.AreEqual(false, redcross.Compatibility(yellowclover));
        }
        [TestMethod]
        public void TestCompabilityForm()
        {
            QColor red = QColor.red;
            QColor yellow = QColor.yellow;
            QShape cross = QShape.cross;
            QShape diamond = QShape.diamond;
            Piece redcross = new Piece(red, cross);
            Piece yellowclover = new Piece(yellow, diamond);
            Piece redclover = new Piece(red, diamond);
            Assert.AreEqual(false, redcross.CompatibilityForm(redclover));
            Assert.AreEqual(true, redclover.CompatibilityForm(yellowclover));
        }
        [TestMethod]
        public void TestCompabilityColor()
        {
            QColor red = QColor.red;
            QColor yellow = QColor.yellow;
            QShape cross = QShape.cross;
            QShape diamond = QShape.diamond;
            Piece redcross = new Piece(red, cross);
            Piece yellowclover = new Piece(yellow, diamond);
            Piece redclover = new Piece(red, diamond);
            Assert.AreEqual(true, redcross.CompatibilityColor(redclover));
            Assert.AreEqual(false, redcross.CompatibilityColor(yellowclover));
        }
        [TestMethod]
        public void Testaddpiece()
        {
            QColor red = QColor.red;
            QColor yellow = QColor.yellow;
            QShape cross = QShape.cross;
            QShape diamond = QShape.diamond;
            Piece redcross = new Piece(red, cross);
            Piece yellowdiamond = new Piece(yellow, diamond);
            Pioche listp = new Pioche();
            listp.Addpieces(yellowdiamond);
            Assert.AreEqual(yellowdiamond, listp.getpieces()[0]);
            listp.Addpieces(redcross);
            Assert.AreEqual(2, listp.getpieces().Count);
            Assert.AreEqual(redcross, listp.getpieces()[1]);
           
        }
        [TestMethod]
        public void TestPioche()
        {
            Pioche listp = new Pioche();
            listp.Piocheinit();
            Assert.AreEqual(108, listp.getpieces().Count);
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace UnitTestProject1
{
    [TestClass]
    public class Testplayer
    {
        [TestMethod]
        public void testplayer()
        {
            List<Piece> hand = new List<Piece>();
            Player Louis = new Player("Louis", 800, 3, hand);
            Assert.AreEqual("Louis", Louis.getname());
            Assert.AreEqual(800, Louis.getscore());
            Assert.AreEqual(3, Louis.getorder());
           
        }
        [TestMethod]
        public void Addpoints()
        {  
            List<Piece> hand = new List<Piece>();
            Player Louis = new Player("Louis", 800, 3, hand);
            Assert.AreEqual(800, Louis.getscore());
            Louis.addpoints(50);
            Assert.AreEqual(850, Louis.getscore());
        }

        [TestMethod]
        public void testinithand()
        {
            AllPlayers listp = new AllPlayers();
            List<Piece> hand = new List<Piece>();
            List<Piece> hand1 = new List<Piece>();
            Pioche pioche = new Pioche();
            Player Louis = new Player("Louis", 800, 2, hand1);
            Player Jean = new Player("Jean", 600, 3, hand);
            listp.Addplayers(Louis);
            listp.Addplayers(Jean);
            Player.inithand(pioche, listp);
            Assert.AreEqual(96, pioche.getpieces().Count);
            Assert.AreEqual(6, hand1.Count);         
        }

        [TestMethod]
        public void testMAJhand()
        {
            AllPlayers listp = new AllPlayers();
            List<Piece> hand = new List<Piece>();
            List<Piece> hand1 = new List<Piece>();
            Pioche pioche = new Pioche();
            Player Louis = new Player("Louis", 800, 2, hand1);
            Player Jean = new Player("Jean", 600, 3, hand);
            listp.Addplayers(Louis);
            listp.Addplayers(Jean);
            Player.inithand(pioche, listp);
            Jean.hand.Remove(hand[1]);
            Assert.AreEqual(5, hand.Count);
            Player.MAJhand(pioche, listp);
            Assert.AreEqual(6, hand.Count);
        }
    }
}

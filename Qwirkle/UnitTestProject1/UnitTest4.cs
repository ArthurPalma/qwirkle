﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Testpieces()
        {
            Color red = Color.red;
            Color yellow = Color.yellow;
            Shape cross = Shape.cross;
            Shape clover = Shape.clover;
            pieces redcross = new pieces (red,cross);

            Assert.AreEqual(cross, redcross.getform());
            Assert.AreEqual(red, redcross.getcolor());
            Assert.AreNotEqual(yellow, redcross.getcolor());
            Assert.AreNotEqual(clover, redcross.getform());
        }

        [TestMethod]
        public void Testaddpiece()
        {
            Color red = Color.red;
            Color yellow = Color.yellow;
            Shape cross = Shape.cross;
            Shape diamond = Shape.diamond;
            pieces redcross = new pieces(red, cross);
            pieces yellowdiamond = new pieces(yellow, diamond);
            listpieces listp = new listpieces();
            listp.Addpieces(yellowdiamond);
            Assert.AreEqual(yellowdiamond, listp.getpieces()[0]);
            listp.Addpieces(redcross);
            Assert.AreEqual(2, listp.getpieces().Count);
            Assert.AreEqual(redcross, listp.getpieces()[1]);
           
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qwirkle;

namespace UnitTestProject1
{
    [TestClass]
    public class TestAllplayers
    {
        [TestMethod]
        public void testaddplayer()
        {
            List<Piece> hand = new List<Piece>();
            List<Piece> hand1 = new List<Piece>();
            AllPlayers listp = new AllPlayers();
            Player Louis = new Player("Louis", 800, 2, hand);
            Player Jean = new Player("Jean", 600, 3, hand1);
            listp.Addplayers(Louis);
            Assert.AreEqual(Louis, listp.getplayers()[0]);
            listp.Addplayers(Jean);
            Assert.AreEqual(2, listp.getplayers().Count);
            Assert.AreEqual(Jean, listp.getplayers()[1]);
        }

        [TestMethod]
        public void testsetscore()
        {
            List<Piece> hand = new List<Piece>();
            AllPlayers listp = new AllPlayers();
            Player Louis = new Player("Louis", 800, 2, hand);
            listp.Addplayers(Louis);
            string expected_introduction = "Louis:800";


            Assert.AreEqual(expected_introduction, listp.setscore());
        }

        [TestMethod]
        public void testwinner()
        {
            AllPlayers listp = new AllPlayers();
            List<Piece> hand = new List<Piece>();
            List<Piece> hand1 = new List<Piece>();
            List<Piece> hand2 = new List<Piece>();
            Player Louis = new Player("Louis", 800, 2, hand);
            Player Jean = new Player("Jean", 600, 3, hand);
            Player Henri = new Player("Henri", 450, 1, hand);
            listp.Addplayers(Louis);
            listp.Addplayers(Jean);
            listp.Addplayers(Henri);
            Assert.AreEqual("Le gagnant est : Louis", listp.getWinner());
        }
    }
}

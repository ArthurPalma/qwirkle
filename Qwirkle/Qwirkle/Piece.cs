﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public enum  QColor 
    {
        red, orange, yellow, green, blue, purple
    }
    public enum QShape
    {
        circle, square, diamond, star, clover, cross
    }
    public class Piece
    {
        public QColor Color
        {
            get;
            set;
        }
        public QShape Shape
        {
            get;
            set;
        }

        public Piece(QColor color, QShape shape)
        {
            this.Color = color;
            this.Shape = shape;
        }

        public QShape getform()
        {
            return this.Shape;
        }
        public QColor getcolor()
        {
            return this.Color;
        }
        public bool Compatibility(Piece piece)
        {
            return (piece != null &&(((this.Color == piece.Color) && (this.Shape != piece.Shape)) ||
                     ((this.Color != piece.Color) && (this.Shape == piece.Shape))));
        }
        public bool CompatibilityColor(Piece piece)
        {
            return (piece != null && (((this.Color == piece.Color) && (this.Shape != piece.Shape))));
        }
        public bool CompatibilityForm(Piece piece)
        {
            return (piece != null &&  (((this.Color != piece.Color) && (this.Shape == piece.Shape))));
        }
    }
}

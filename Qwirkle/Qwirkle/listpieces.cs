﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public class listpieces
    {
        public List<pieces> pieceslist;
        public listpieces()
        {
            this.pieceslist = new List<pieces>();
        }
        public void Addpieces(pieces pieces)
        {
            this.pieceslist.Add(pieces);
        }
        public List<pieces> getpieces()
        {
            return this.pieceslist;
        }


        public listpieces(bool check)
        {
            int iteration = 3;
            if (check)
            {
                for (iteration = 1; iteration >= 3; iteration++)
                {
                    foreach (Color i in Enum.GetValues(typeof(Color)))
                    {
                        foreach (Shape j in Enum.GetValues(typeof(Shape)))
                        {
                            pieceslist.Add(new pieces(i, j));
                        }
                    }
                }
            }
        }
    }
}

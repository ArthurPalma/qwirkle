﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public class Pioche
    {
        public List<Piece> pieceslist;

        public Pioche()
        {
            this.pieceslist = new List<Piece>();
        }
        public void Piocheinit()
        {
            
            for (int iteration = 1; iteration <= 3; iteration++)
                {
                    foreach (QColor i in Enum.GetValues(typeof(QColor)))
                    {
                        foreach (QShape j in Enum.GetValues(typeof(QShape)))
                        {
                            this.pieceslist.Add(new Piece(i, j));
                        }
                    }

                }
        }
        public void Addpieces(Piece pieces)
        {
            this.pieceslist.Add(pieces);
        }

        public List<Piece> getpieces()
        {
            return this.pieceslist;
        }

    }
}

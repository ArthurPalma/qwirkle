﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public class Board
    {
        public int RowCount
        {
            get;
            private set;
        }
        public int ColumnCount
        {
            get;
            private set;
        }

        private pieces[,] _board;

        public Board(int rowCount, int columnCount)
        {
            this.RowCount = rowCount;
            this.ColumnCount = columnCount;

            this._board = new pieces[rowCount, columnCount];
        }
        public bool OffBoardPosition(int row, int column)
        {
            return ((row < 0 || row >= RowCount) || (column < 0 || column >= ColumnCount));
        }
        public void Place(pieces tile, int row, int column)
        {
           
            if (_board[row, column] == null && OffBoardPosition(row, column))
            {
                _board[row, column] = tile;
            }

            _board[row, column] = tile;
        }
        public bool IsValidPlacement(pieces tile, int row, int column)
        {
            if (OffBoardPosition(row, column))
            {
                return false;
            }
            if (_board[row, column] != null)
            {
                return false;
            }
            bool potentialHorizontal = (!OffBoardPosition(row, column - 1) && tile.Compatibility(_board[row, column - 1])) ||
                                       (!OffBoardPosition(row, column + 1) && tile.Compatibility(_board[row, column + 1]));

            bool potentialVertical = (!OffBoardPosition(row - 1, column) && tile.Compatibility(_board[row - 1, column])) ||
                                     (!OffBoardPosition(row + 1, column) && tile.Compatibility(_board[row + 1, column]));

            _board[row, column] = tile;

            if (potentialHorizontal)
            {
                var rowGroup = GetRowGroupTiles(row, column);
                if (rowGroup.Distinct().Count() != rowGroup.Count())
                {
                    _board[row, column] = null;

                    return false;
                }
            }
            if (potentialVertical)
            {
                var columnGroup = GetColumnGroupTiles(row, column);
                if (columnGroup.Distinct().Count() != columnGroup.Count())
                {
                    _board[row, column] = null;

                    return false;
                }
            }
            _board[row, column] = null;
            return true;
        }
    }
}

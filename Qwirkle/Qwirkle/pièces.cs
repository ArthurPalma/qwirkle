﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qwirkle
{
    public enum Color
    {
        red, orange, yellow, green, blue, purple
    }
    public enum Shape
    {
        circle, square, diamond, star, clover, cross
    }
    public class pieces
    {
        public Color Color
        {
            get;
            private set;
        }
        public Shape Shape
        {
            get;
            private set;
        }

        public pieces(Color color, Shape shape)
        {
            this.Color = color;
            this.Shape = shape;
        }

        public Shape getform()
        {
            return this.Shape;
        }
        public Color getcolor()
        {
            return this.Color;
        }
        public bool Compatibility(pieces piece)
        {
            return (piece != null &&(((this.Color == piece.Color) && (this.Shape != piece.Shape)) ||
                     ((this.Color != piece.Color) && (this.Shape == piece.Shape))));
        }
    }
}
